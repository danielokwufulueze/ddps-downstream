#!/bin/bash

# Replace "https://" with "" in ${COMPONENT_REPOSITORY_URL}
  COMPONENT_REPOSITORY=$(echo "${COMPONENT_REPOSITORY_URL}" | sed 's~https:\/\/~~g');
  COMPONENT_REPOSITORY_URL="https://gitlab-ci-token:${ACCESS_TOKEN}@${COMPONENT_REPOSITORY}";

git clone \
  -b ${COMPONENT_BRANCH_SLUG} \
  ${COMPONENT_REPOSITORY_URL} \
  ${COMPONENT_DIRECTORY};

imageForType=client;

if [[ "${IS_API}" == "yes" ]]
then
  imageForType=api;
fi

dockerfileName="$CI_PROJECT_DIR/Dockerfile-build-${imageForType}";

cp "${dockerfileName}" ${COMPONENT_DIRECTORY};

cd ${COMPONENT_DIRECTORY};

docker build \
  --tag ${BUILD_IMAGE_PATH} \
  -f "${dockerfileName}" \
  .;

# The ${COMPONENT_WORK_DIRECTORY} is the directory from which the
# component can be directly built.
# Its value is ${COMPONENT_DIRECTORY}/${COMPONENT_WORK_DIRECTORY}.
# For example, the directory from which one can directly run the
# following:
#   dotnet build
#   yarn build
# This directory is expected to be a sibling to a public/ directory
# (${PUBLIC_ARTIFACTS_PATH}) where the built artifacts will reside
# alongside the Dockerfile responsible for making a built image of the
# upstream application.

# Run a container to build the upstream application's artifacts.
# These artifacts are saved in the upstream application's public
# directory (${PUBLIC_ARTIFACTS_PATH}).
# The value of ${PUBLIC_ARTIFACTS_PATH} is ${COMPONENT_DIRECTORY}/public
docker run --rm \
  -e COMPONENT_WORK_DIRECTORY \
  -v ${PUBLIC_ARTIFACTS_PATH}:/app/public \
  ${BUILD_IMAGE_PATH};
